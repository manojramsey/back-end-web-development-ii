const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
const bookRouter = express.Router();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

const db = mongoose.connect("mongodb://localhost:27017/library");
const Book = require("./models/bookModel");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

bookRouter
  .route("/books")
  .post((req, res) => {
    const book = new Book(req.body);
    book.save();

    return res.status(201).json(book);
  })

  .get((req, res) => {
    const query = {};
    if (req.query.author) {
      query.author = req.query.author;
    }

    Book.find(query, (error, books) => {
      if (error) {
        return res.send(error);
      }
      return res.json(books);
    });
  });

bookRouter
  .route("/books/:bookId")
  .get((req, res) => {
    const query = {};
    if (req.query.author) {
      query.author = req.query.author;
    }

    Book.findById(req.params.bookId, (error, book) => {
      if (error) {
        return res.send(error);
      }
      return res.json(book);
    });
  })

  //PUT
  .put((req, res) => {
    Book.findById(req.params.bookId, (error, book) => {
      if (error) {
        return res.send(error);
      }
      book.author = req.body.author;
      book.title = req.body.title;
      book.save();
      return res.json(book);
    });
  })

  //PATCH
  .patch((req, res) => {
    Book.findById(req.params.bookId, (error, book) => {
      if (error) {
        return res.send(error);
      }
      // remove _id
      if (req.body._id) {
        delete req.body._id;
      }

      //loop through body and update book
      Object.entries(req.body).forEach(item => {
        const key = item[0];
        const value = item[1];
        book[key] = value;
      });

      //save book

      book.save(error => {
        if (error) {
          res.send(error);
        }
        return res.json(book);
      });
    });
  })

  //delete
  .delete((req, res) => {
    Book.findById(req.params.bookId, (error, book) => {
      book.remove(error => {
        if (error) {
          return res.send(error);
        }
        return res.sendStatus(204);
      });
    });
  });

app.use("/api", bookRouter);

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
