const express = require("express"),
  port = 3000,
  app = express(),
  morgan = require("morgan"),
  bodyParser = require("body-parser"),
  cors = require("cors");
homeController = require("./controllers/homeController");
moviesController = require("./controllers/moviesController");
errorController = require("./controllers/errorController");

app.set("view engine", "pug");
app.set("port", process.env.PORT || 3000);
app.use(express.static("public"));
app.use(morgan("short"));
app.use(bodyParser.json());
app.use(cors());

app.get("/", homeController.sendReq);
app.get("/user/:name", homeController.sendReqParam);
app.get("/movies", moviesController.sendReq);
app.get("/movies/:name", moviesController.sendReqParam);
app.post("/movies/:name/:year", moviesController.sendPostReq);
app.post("/signup", moviesController.sendPostSubscribe);

app.use(errorController.respondNoResourceFound);
app.use(errorController.respondInternalError);

//set application to listen to port
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
