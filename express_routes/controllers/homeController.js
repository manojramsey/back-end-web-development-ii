exports.sendReq = (req, res) => {
  res.render("index", {
    title: "Movie Rentals",
    message: "Welcome to Movie Rentals"
  });
};

exports.sendReqParam = (req, res) => {
  let user = req.params;
  res.render("index", {
    title: `Hello ${user.name}`,
    message: `Welcome to Movie Rentals ${user.name.toUpperCase()} `
  });
};
