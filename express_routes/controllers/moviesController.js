exports.sendReq = (req, res) => {
  res.render("movies");
};

exports.sendReqParam = (req, res) => {
  let movie = req.params;
  res.send(`information about movie ${movie.name}`);
};

exports.sendPostReq = (req, res) => {
  let movie = req.params;
  res.send(`You have posted movie name: ${movie.name}, year: ${movie.year}`);
};

exports.sendPostSubscribe = (req, res) => {
  console.log(req.body);
  res.status(200).send({ message: "Data Received" });
};
