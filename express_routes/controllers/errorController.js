const httStatus = require("http-status-codes");

exports.respondNoResourceFound = (req, res) => {
  let errorCode = httStatus.NOT_FOUND;
  //res.send(`${errorCode} | This Page dose not exist!`);
  res.sendFile(`./public/html/${errorCode}.html`, { root: "./" });
};

exports.respondInternalError = (error, req, res, next) => {
  let errorCode = httStatus.INTERNAL_SERVER_ERROR;
  console.log(`Error Occurred ${error}`);
  res.status(errorCode);
  res.send(`${errorCode} | Sorry, Our application is experiencing a problem `);
};
